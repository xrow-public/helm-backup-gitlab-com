#!/bin/bash


# Fail on any error for monitoring
set -e

PATH="$PATH:$HOME/bin"

# Set your GitLab token
if [[ -z "$GITLAB_TOKEN" ]]; then
  echo "Please set the GITLAB_TOKEN environment variable"
  exit 1
fi

dnf install jq -y

dir="/mnt/backup"

# Initialize page number
page=1

# Loop over pages
while true; do
  # Fetch projects for the current page
  projects=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects?owned=true&simple=true&per_page=100&page=$page")

  # If no projects were returned, we're done
  if [ "$(echo "$projects" | jq '. | length')" -eq 0 ]; then
    break
  fi

  # Loop over the projects
  echo "$projects" | jq -r '.[] | .id, .path_with_namespace' | while read -r id; read -r path; do
    echo "Exporting project $path with ID $id"

    # Start the export
    curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/$id/export"

    # Wait for the export to finish
    while true; do
      response=$(curl --request GET --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/$id/export")
      if echo "$response" | grep -q "\"export_status\":\"finished\""; then
        break
      fi
      echo "Waiting for export of project $path to finish..."
      sleep 10
    done
    mkdir -p ${dir}/$path
    rm -rf ${dir}/$path/project.tar.gz
    # Download the export 
    curl --request GET --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/$id/export/download" --output "${dir}/$path/project.tar.gz"
  done

  # Move to the next page
  page=$((page + 1))
done