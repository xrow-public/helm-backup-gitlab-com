# Helm Chart for the backup of Gitlab.com (Your Stuff)

* Set the token variable in values.yaml
* Install the chart
* Check the cron, if it backups

```bash
helm upgrade --install backup \
  oci://registry.gitlab.com/xrow-public/helm-backup-gitlab-com/charts/backup-gitlab-com \
  --version $CI_DOCUMENTATION_LATEST_TAG \
  -n backup-gitlab --create-namespace \
  --set token=$GITLAB_TOKEN
```
Important: 

The chart was created under the assumption that your kubernetes has automatic regulary backups. If you need help, let us know. 

## Getting started

```bash
podman run -it -u root -v $PWD:/scripts:z -e GITLAB_TOKEN=$GITLAB_TOKEN registry.access.redhat.com/ubi9/ubi:9.3-1552 sh -c "sh /scripts/backup.sh"
```
